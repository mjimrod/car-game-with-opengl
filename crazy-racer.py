# Autor: Mitchel Jimenez
# coding=utf-8

# Import de librerias
import glfw
from OpenGL.GL import *
import OpenGL.GL.shaders
import numpy as np
import sys
from PIL import Image

# Impor de archivos necesarios
import transformations as tr
import easy_shaders as es
import scene_graph as sg
import basic_shapes as bs
import lighting_shaders as ls
import ex_curves as cv

# We will use 32 bits data, so we have 4 bytes
# 1 byte = 8 bits
SIZE_IN_BYTES = 4

"""
________________________________________________________________________________
CONTROLLER: Manejo de la clase Controller que actualiza el modelo segun los
            keyboard events
________________________________________________________________________________
"""

# A class to store the application control
class Controller:
    def __init__(self):
        self.fillPolygon = True
        self.autoPhi = np.pi/2 # angulo de vista
        self.autoPos = [0.5, -7, 4]
        self.dt = 0
        self.carTriangle = None # Triangulo dentro del cual esta el auto

# We will use the global controller as communication with the callback function
controller = Controller()

# Funcion que mapea la interaccion del usuario con el controlador y a su vez,
# el modelo del programa
def on_key(window, key, scancode, action, mods):

    if action != glfw.PRESS:
        return

    global controller

    if key == glfw.KEY_SPACE:
        controller.fillPolygon = not controller.fillPolygon

    elif key == glfw.KEY_ESCAPE:
        glfw.set_window_should_close(window, True)

"""
________________________________________________________________________________
MODEL: Creacion de todas las GPUShapes y grafo de escena, manejo de info de los
objetos creados, a la vez que contenemos a la clase RoundedCurve
________________________________________________________________________________
"""

# AUTO
# ______________________________________________________________________________
def createCar(r, g, b):
    gpuBlackCube = es.toGPUShape(bs.createColorNormalsCube(0, 0, 0))
    gpuChasisCube = es.toGPUShape(bs.createColorNormalsCube(r, g, b))
    gpuLightsCube = es.toGPUShape(bs.createColorNormalsCube(1,1,0))
    gpuWindow = es.toGPUShape(bs.createColorNormalsCube(0,1,1))
    gpuExhaustPipe = es.toGPUShape(bs.createColorNormalsCube(0.27,0.29,0.31))
    gpuBumper = es.toGPUShape(bs.createColorNormalsCube(0.25,0.29,0.31))
    gpuMotor = es.toGPUShape(bs.createColorNormalsCube(0.86,0.86,0.86))
    gpuAntenna = es.toGPUShape(bs.createColorNormalsCube(0,0,0))
    # Cheating a single wheel
    wheel = sg.SceneGraphNode("wheel")
    wheel.transform = tr.scale(0.2, 0.8, 0.2)
    wheel.childs += [gpuBlackCube]

    wheelRotation = sg.SceneGraphNode("wheelRotation")
    wheelRotation.childs += [wheel]

    # Instanciating 2 wheels, for the front and back parts
    frontWheel = sg.SceneGraphNode("frontWheel")
    frontWheel.transform = tr.translate(0.3, 0, -0.3)
    frontWheel.childs += [wheelRotation]

    backWheel = sg.SceneGraphNode("backWheel")
    backWheel.transform = tr.translate(-0.3, 0, -0.3)
    backWheel.childs += [wheelRotation]

    # Creating the lights
    rightlight= sg.SceneGraphNode("rightlight")
    rightlight.transform=tr.matmul([tr.scale(0.1,0.1,0.1),tr.translate(0.0, 4, 0.0)])
    rightlight.childs += [gpuLightsCube]

    leftlight = sg.SceneGraphNode("leftlight")
    leftlight.transform = tr.matmul([tr.scale(0.1,0.1,0.1),tr.translate(0.0, -0.8, 0.0)])
    leftlight.childs += [gpuLightsCube]

    lights = sg.SceneGraphNode("lights")
    lights.transform = tr.translate(0.5, -0.15, 0.1)
    lights.childs += [leftlight]
    lights.childs += [rightlight]

    # Creating window
    windowf= sg.SceneGraphNode("windowf")
    windowf.transform=tr.matmul([tr.scale(0.5,0.6,0.2),tr.translate(-0.1,0.0,1.8)])
    windowf.childs += [gpuWindow]

    windowb = sg.SceneGraphNode("windowb")
    windowb.transform = tr.matmul([tr.scale(0.3, 0.6, 0.2), tr.translate(-1.0, 0.0, 1.8)])
    windowb.childs += [gpuWindow]

    windowsi = sg.SceneGraphNode("windowb")
    windowsi.transform = tr.matmul([tr.scale(0.25, 0.9, 0.2), tr.translate(-0.4, 0.0, 1.8)])
    windowsi.childs += [gpuWindow]

    window = sg.SceneGraphNode("window")
    window.childs += [windowf]
    window.childs += [windowb]
    window.childs += [windowsi]
    # Creating the upper chasis body of the car
    upperchasisbody = sg.SceneGraphNode("upperchasisbody")
    upperchasisbody.transform = tr.matmul([tr.scale(0.6, 0.8, 0.4), tr.translate(-0.2, 0.0, 0.8)])
    upperchasisbody.childs += [gpuChasisCube]

    # Creating the antenna
    antenna= sg.SceneGraphNode("antenna")
    antenna.transform=tr.matmul([tr.scale(0.005, 0.005, 0.4), tr.translate(-50, -45, 1.5)])
    antenna.childs += [gpuAntenna]

    # Creating the upper chasis of the car
    upperchasis=sg.SceneGraphNode("upperchasis")
    upperchasis.childs += [upperchasisbody]
    upperchasis.childs += [window]
    upperchasis.childs += [antenna]
    # Creating the exhaust pipe of the car
    exhaustpipe=sg.SceneGraphNode("exhaustpipe")
    exhaustpipe.transform=tr.matmul([tr.scale(0.4,0.2,0.1),tr.translate(-1.0,-0.5,-0.1)])
    exhaustpipe.childs += [gpuExhaustPipe]

    # Creating the down chasis body of the car
    downchasisbody = sg.SceneGraphNode("downchasis")
    downchasisbody.transform = tr.scale(1, 0.7, 0.5)
    downchasisbody.childs += [gpuChasisCube]

    # Creating the bumper of the car
    bumper= sg.SceneGraphNode("bumper")
    bumper.transform=tr.matmul([tr.scale(0.2,0.9,0.1),tr.translate(2.2,0.0,-1.2)])
    bumper.childs += [gpuBumper]

    # Creating the motor of the car
    motor=sg.SceneGraphNode("motor")
    motor.transform=tr.matmul([tr.scale(0.2,0.2,0.1),tr.translate(1.85,0.0,2.8)])
    motor.childs += [gpuMotor]

    # Creating the down chasis of the car
    downchasis = sg.SceneGraphNode("downchasis")
    downchasis.childs += [downchasisbody]
    downchasis.childs += [exhaustpipe]
    downchasis.childs += [bumper]
    downchasis.childs += [motor]
    # Creating the down chasis of the car
    bodychasis = sg.SceneGraphNode("bodychasis")
    bodychasis.childs += [downchasis]
    bodychasis.childs += [upperchasis]

    chasis = sg.SceneGraphNode("chasis")
    chasis.childs += [bodychasis]
    chasis.childs += [lights]
    # All pieces together
    car = sg.SceneGraphNode("car")
    car.childs += [chasis]
    car.childs += [frontWheel]
    car.childs += [backWheel]

    return car

# CURVA
# ______________________________________________________________________________

# Clase Triangle (triangulo). Esta clase nos ayudara a crear la malla de trian-
# gulos. Tiene asociado los triangulos vecinos, los vertices, la normal del
# triangulo y la velocidad asociada a ese "segmento"
class Triangle:
    def __init__(self):
        self.nearTriangles = np.array([None, None, None], dtype = 'object')
        self.vertices = np.empty([3, 3], dtype = np.float64) # Float de 64 bits
        self.velocity = np.empty(3, dtype = np.float64)

    # Metodo que retorna un punto proyectado sobre el triangulo en caso de que
    # el punto pueda proyectarse sobre el. Es basicamente interseccion de una
    # linea recta paralela al eje Z. Retorna None si es que la recta no inter-
    # secta el triangulo.
    def puntoProyectado(self, q1):
        # Puntos a usar
        p1 = self.vertices[0]
        p2 = self.vertices[1]
        p3 = self.vertices[2]
        q2 = np.array([q1[0], q1[1], -1000])

        def signed_tetra_volume(a, b, c, d):
            return np.sign(np.dot(np.cross(b - a, c - a), d - a) / 6.0)

        s1 = signed_tetra_volume(q1, p1, p2, p3)
        s2 = signed_tetra_volume(q2, p1, p2, p3)

        if s1 != s2:
            s3 = signed_tetra_volume(q1, q2, p1, p2)
            s4 = signed_tetra_volume(q1, q2, p2, p3)
            s5 = signed_tetra_volume(q1, q2, p3, p1)
            if s3 == s4 and s4 == s5:
                n = np.cross(p2 - p1, p3 - p1)
                t = np.dot(p1 - q1, n) / np.dot(q2 - q1, n)
                return q1 + t * (q2 - q1)
        return None

    # ESTA SECCION FUE DESECHADA PUESTO QUE NO ES USADA, SIN EMBARGO, POR
    # COMPLETITUD SE DEJA.

    # Me entrega la altura de un determinado punto sobre un triangulo que des-
    # cribe una superficie triangular
    # def getHeight(self, pos):
    #     det = (self.vertices[1][1] - self.vertices[2][1]) * (self.vertices[0][0] - self.vertices[2][0]) + (self.vertices[2][0] - self.vertices[1][0]) * (self.vertices[1][1] - self.vertices[2][1])
    #     l1 = ((self.vertices[1][1] - self.vertices[2][1]) * (pos[0] - self.vertices[2][0]) + (self.vertices[2][0] - self.vertices[1][0]) * (pos[1] - self.vertices[2][1])) / det
    #     l2 = ((self.vertices[2][1] - self.vertices[1][1]) * (pos[0] - self.vertices[2][0]) + (self.vertices[0][0] - self.vertices[2][0]) * (pos[1] - self.vertices[2][1])) / det
    #     l3 = 1. - l1 - l2
    #     return l1 * self.vertices[0][2] + l2 * self.vertices[1][2] + l3 * self.vertices[2][2]
    #
    # # Entrega la velocidad de un determinado punto sobre de un triangulo
    # def getVelocity(self, velocitymap):
    #     det = (self.vertices[1][1] - self.vertices[2][1]) * (self.vertices[0][0] - self.vertices[2][0]) + (self.vertices[2][0] - self.vertices[1][0]) * (self.vertices[1][1] - self.vertices[2][1])
    #     l1 = ((self.vertices[1][1] - self.vertices[2][1]) * (pos[0] - self.vertices[2][0]) + (self.vertices[2][0] - self.vertices[1][0]) * (pos[1] - self.vertices[2][1])) / det
    #     l2 = ((self.vertices[2][1] - self.vertices[1][1]) * (pos[0] - self.vertices[2][0]) + (self.vertices[0][0] - self.vertices[2][0]) * (pos[1] - self.vertices[2][1])) / det
    #     l3 = 1. - l1 - l2
    #     return l1 * velocitymap[self.vertices[0][0], self.vertices[0][1]] + l2 * velocitymap[self.vertices[1][0], self.vertices[1][1]] + l3 * velocitymap[self.vertices[2][0], self.vertices[2][1]]
    #
    # # Metodo que actualiza la normal del triangulo y la retorna
    # def getNormal(self):
    #     # Vectores
    #     a = self.vertices[1] - self.vertices[0]
    #     b = self.vertices[2] - self.vertices[0]
    #     return np.cross(a, b)

# Clase Node (nodo). Cada nodo sera un punto que forme parte de la curva rounded
# non-uniform spline
class Node:
    def __init__(self):
        self.position = None
        self.velocity = None
        self.distance_next_node = 0

# Hacemos una clase que nos ayude a manejar la curva, donde nodesQuantity es la
# cantidad de nodos que tendra la curva y maxDistance es el largo de la curva,
# que seria basicamente la suma de los segmentos entre cada uno de los nodos.
class RoundedCurve:
    def __init__(self, nodesQuantity):
        self.nodes = np.empty(nodesQuantity, dtype = 'object')
        self.maxDistance = 0
        self.Shape = None
        self.heightmap = {}
        self.velocitymap = {}
        self.triangleMesh = []

    # Este metodo me permite modificar los nodos o mejor dicho ingresarlos a la
    # curva, usando el indice correspondiente.
    def addNode(self, position, index):
        node = Node()
        node.position = np.array([[position[0], position[1], position[2]]])
        self.nodes[index] = node
        self.heightmap[position[0], position[1]] = position[2]

    # Este metodo actualiza las velocidades segun los nodos ingresados
    # OJO: Para que esto funcione bien, la curva debe formar un circuito cerrado
    # En caso de que no sea asi, habria que definir las velocidades de los nodos
    # inicio y final. Tambien, aprovechando el ciclo, hacemos update a las dist-
    # ancias de los segmentos.
    def updateVelocitiesAndDistances(self):
        maxDistance = 0
        for i in range(len(self.nodes)):
            # Creo dos vectores, uno hacia el punto anterior del circuito y uno
            # hacia el punto siguiente del circuito curvo.

            # Estos ifs ayudan a decidir en los casos bordes de nuestro arreglo
            # de nodos. Esto no hubiese sido necesario en el caso de haber
            # creado una estructura linked lists circular, sin embargo ya se
            # habia implementado la solucion con nodos y pues no se quiere cambiar.
            if i == 0:
                a = self.nodes[len(self.nodes) - 1].position - self.nodes[i].position
                b = self.nodes[i + 1].position - self.nodes[i].position
                self.nodes[i].distance = np.linalg.norm(self.nodes[i + 1].position - self.nodes[i].position)
            elif i == len(self.nodes) - 1:
                a = self.nodes[i - 1].position - self.nodes[i].position
                b = self.nodes[0].position - self.nodes[i].position
                self.nodes[i].distance = np.linalg.norm(self.nodes[0].position - self.nodes[i].position)
            else:
                a = self.nodes[i - 1].position - self.nodes[i].position
                b = self.nodes[i + 1].position - self.nodes[i].position
                self.nodes[i].distance = np.linalg.norm(self.nodes[i + 1].position - self.nodes[i].position)

            # Sumo las distancias a la variable auxiliar maxDistance
            maxDistance += self.nodes[i].distance

            # Cabe destacar que al final tambien estamos haciendo update a las
            # distancias entre los nodos.

            # Ahora calculamos el vector bisector del angulo formado entre ambos
            # vectores
            c = (a / np.linalg.norm(a)) + (b / np.linalg.norm(b))

            # Aplico producto cruz a c y a
            d = np.cross(c, a)

            # Luego para obtener la direccion de la velocidad, aplico producto
            # cruz a c y d, obteniendo un vector con direccion tangente a la
            # curva. Ademas, lo normalizo por si acaso.
            v = np.cross(c, d)
            v = v / np.linalg.norm(v)

            # Lo asigno directamente al atributo velocidad del nodo
            self.nodes[i].velocity = np.array([[v[0][0], v[0][1], v[0][2]]])
            # Ingresamos la velocidad al velocitymap
            self.velocitymap[self.nodes[i].position[0][0], self.nodes[i].position[0][1]] = self.nodes[i].velocity
        # Actualizo el atributo maxDistance
        self.maxDistance = maxDistance

        return None

    # OJO: ESTE METODO NO ES USADO EN ESTA IMPLEMENTACION
    # Este metodo nos entrega un np.array de shape (3) que contiene la posicion
    # sobre la curva en un determinado tiempo time
    def getPosition(self, time):
        distance = time * self.maxDistance
        currentDistance = 0

        i = 0
        while currentDistance + self.nodes[i].distance_next_node < distance and i < len(self.nodes):
            currentDistance += self.nodes[i].distance_next_node
            i += 1

        # i es el numero del segmento y t es el tiempo sobre ese segmento
        t = distance - currentDistance

        # Escalamos a t en el rango [0, 1]
        t /= self.nodes[i].distance_next_node

        # Obtenemos la velocidad inicial y final
        startVelocity = self.nodes[i].velocity * self.nodes[i].distance_next_node
        endVelocity = self.nodes[i + 1].velocity * self.nodes[i].distance_next_node

        M = cv.hermiteMatrix(self.nodes[i].position, self.nodes[i + 1].position, startVelocity, endVelocity)
        T = np.array([1, t, t*t, t*t*t], dtype = np.float32).T

        return np.matmul(M, T).T
        # La posicion retornada esta dispuesto como vector columna, asi que para
        # acceder a xyz seria [0][0], [0][1] y [0][2] respectivamente (OJO)

    # Este metodo transforma la curva en una malla triangular en forma de
    # GPUShape.
    def updateShape(self, color):
        meshVertices = []
        meshIndices = []
        for i in range(len(self.nodes)):

            if i == len(self.nodes) - 1:
                P0 = self.nodes[i].position
                P1 = self.nodes[0].position
                T0 = self.nodes[i].velocity
                T1 = self.nodes[0].velocity
            else:
                P0 = self.nodes[i].position
                P1 = self.nodes[i + 1].position
                T0 = self.nodes[i].velocity
                T1 = self.nodes[i + 1].velocity
            GMb = cv.hermiteMatrix(P0.T, P1.T, T0.T, T1.T)
            # Nueva funcion que evalua la curva
            cv.evalCurveMesh(GMb, 3, meshVertices, color, self.heightmap, self.velocitymap)

        j = 0
        k0 = 0

        while j < len(meshVertices) / 9 and (k0 + (9 * 3)) < len(meshVertices):
            cero = j
            uno = j + 1
            dos = j + 3
            tres = j + 2
            meshIndices += [cero, uno, dos, dos, tres, cero]

            # Calculo de indice en meshVertices
            k1 = 9 + k0
            k2 = 9 * 3 + k0
            k3 = 9 * 2 + k0

            # Triangulos
            triangleOne = Triangle()

            triangleOne.vertices[0][0] = meshVertices[k0]
            triangleOne.vertices[0][1] = meshVertices[k0 + 1]
            triangleOne.vertices[0][2] = meshVertices[k0 + 2]

            triangleOne.vertices[1][0] = meshVertices[k1]
            triangleOne.vertices[1][1] = meshVertices[k1 + 1]
            triangleOne.vertices[1][2] = meshVertices[k1 + 2]

            triangleOne.vertices[2][0] = meshVertices[k2]
            triangleOne.vertices[2][1] = meshVertices[k2 + 1]
            triangleOne.vertices[2][2] = meshVertices[k2 + 2]

            triangleOne.velocity[0] = self.velocitymap[meshVertices[k0], meshVertices[k0 + 1]][0][0]
            triangleOne.velocity[1] = self.velocitymap[meshVertices[k0], meshVertices[k0 + 1]][0][1]
            triangleOne.velocity[2] = self.velocitymap[meshVertices[k0], meshVertices[k0 + 1]][0][2]

            triangleTwo = Triangle()

            triangleTwo.vertices[0][0] = meshVertices[k2]
            triangleTwo.vertices[0][1] = meshVertices[k2 + 1]
            triangleTwo.vertices[0][2] = meshVertices[k2 + 2]

            triangleTwo.vertices[1][0] = meshVertices[k3]
            triangleTwo.vertices[1][1] = meshVertices[k3 + 1]
            triangleTwo.vertices[1][2] = meshVertices[k3 + 2]

            triangleTwo.vertices[2][0] = meshVertices[k0]
            triangleTwo.vertices[2][1] = meshVertices[k0 + 1]
            triangleTwo.vertices[2][2] = meshVertices[k0 + 2]

            # Los anhadimos al arreglo de triangulos
            self.triangleMesh.append(triangleOne)
            self.triangleMesh.append(triangleTwo)

            j += 2
            k0 += 18

        self.Shape = bs.Shape(meshVertices, meshIndices)
        return None

    # Este metodo hace update de los vecinos de la triangleMesh
    def updateTriangleMeshVecinity(self):
        final = len(self.triangleMesh) - 1
        i = 0
        while i < len(self.triangleMesh):
            if i == 0:
                self.triangleMesh[i].nearTriangles[1] = self.triangleMesh[i + 1]
                self.triangleMesh[i].nearTriangles[2] = self.triangleMesh[final]
                self.triangleMesh[i + 1].nearTriangles[1] = self.triangleMesh[i]
                self.triangleMesh[i + 1].nearTriangles[2] = self.triangleMesh[i + 2]
            elif (i + 1) == final:
                self.triangleMesh[i].nearTriangles[1] = self.triangleMesh[final]
                self.triangleMesh[i].nearTriangles[2] = self.triangleMesh[i - 1]
                self.triangleMesh[final].nearTriangles[1] = self.triangleMesh[i]
                self.triangleMesh[final].nearTriangles[2] = self.triangleMesh[0]
            else:
                self.triangleMesh[i].nearTriangles[1] = self.triangleMesh[i + 1]
                self.triangleMesh[i].nearTriangles[2] = self.triangleMesh[i - 1]
                self.triangleMesh[i + 1].nearTriangles[1] = self.triangleMesh[i]
                self.triangleMesh[i + 1].nearTriangles[2] = self.triangleMesh[i + 2]
            i += 2
        return None

    # Es un metodo que busca el triángulo sobre el cual el carro esta en un
    # principio
    def firstCarSearch(self):
        pos = np.array([controller.autoPos[0], controller.autoPos[1], controller.autoPos[2]])
        for i in range(len(self.triangleMesh)):
            p = self.triangleMesh[i].puntoProyectado(pos)
            if type(p) != type(None):
                controller.carTriangle = self.triangleMesh[i]
                controller.autoPos[2] = p[2] + 0.7
        return None

    # Este metodo busca el triangulo sobre el cual podria estar el auto despues
    # de haberse movido. La idea es que se busca de forma iterativa en 20
    # triangulos hacia adelante y hacia atras del triangulo sobre el que esta
    # actualmente el auto
    def searchForCar(self, triangle):
        p = triangle.puntoProyectado(np.array([controller.autoPos[0], controller.autoPos[1], controller.autoPos[2]]))
        if type(p) != type(None):
            controller.carTriangle = triangle
            controller.autoPos[2] = p[2] + 0.7
        return None

# Inicializamos la curva con un numero de puntos y un parametro correspondiente
# al largo total de la curva
roundedCurve = RoundedCurve(32)

# Ahora hay que ingresar a mano la position de cada uno de estos nodos
# INICIO
roundedCurve.addNode(np.array([  0,   0,   0], dtype = np.float32), 0)
# COMIENZO DE BAJADA
roundedCurve.addNode(np.array([  1,    5,  -5], dtype = np.float32), 1)
roundedCurve.addNode(np.array([  1,   10, -10], dtype = np.float32), 2)
roundedCurve.addNode(np.array([  2,   15, -11], dtype = np.float32), 3)
roundedCurve.addNode(np.array([  2,   25, -12], dtype = np.float32), 4)
roundedCurve.addNode(np.array([  1,   40, -13], dtype = np.float32), 5)
roundedCurve.addNode(np.array([  1,   80, -14], dtype = np.float32), 6)
roundedCurve.addNode(np.array([  2,  100, -15], dtype = np.float32), 7)
roundedCurve.addNode(np.array([  2,  115, -14], dtype = np.float32), 8)
roundedCurve.addNode(np.array([  1,  140, -13], dtype = np.float32), 9)
roundedCurve.addNode(np.array([  1,  170, -12], dtype = np.float32), 10)
roundedCurve.addNode(np.array([  2,  205, -11], dtype = np.float32), 11)
roundedCurve.addNode(np.array([  2,  220, -10], dtype = np.float32), 12)
# SUBIDA
roundedCurve.addNode(np.array([   0,  245,  -5], dtype = np.float32), 13)
roundedCurve.addNode(np.array([   0,  270,  -3], dtype = np.float32), 14)
roundedCurve.addNode(np.array([   1,  295,   1], dtype = np.float32), 15)
# PLANO
roundedCurve.addNode(np.array([  1,  300,  1.8], dtype = np.float32), 16)
# CURVA DE REGRESO
roundedCurve.addNode(np.array([  50, 450,  1.8], dtype = np.float32), 17)
roundedCurve.addNode(np.array([  160, 410,  1.8], dtype = np.float32), 18)
roundedCurve.addNode(np.array([  95, 350,  1.8], dtype = np.float32), 19)
# NUEVA SUBIDA
roundedCurve.addNode(np.array([  75,  320,   3], dtype = np.float32), 20)
roundedCurve.addNode(np.array([  74,  315,   5], dtype = np.float32), 21)
roundedCurve.addNode(np.array([  74,  300,  10], dtype = np.float32), 22)
roundedCurve.addNode(np.array([  75,  280,  15], dtype = np.float32), 23)
roundedCurve.addNode(np.array([  75,  250,  20], dtype = np.float32), 24)
# BAJADA A PLANO
roundedCurve.addNode(np.array([  76,  200,  15], dtype = np.float32), 25)
roundedCurve.addNode(np.array([  76,  140,   5], dtype = np.float32), 26)
roundedCurve.addNode(np.array([  75,  100, 1.8], dtype = np.float32), 27)
# RECTA FINAL
roundedCurve.addNode(np.array([  75, -20,  0.5], dtype = np.float32), 28)
# CONEXION FINAL
roundedCurve.addNode(np.array([  50,  -60,  0.5], dtype = np.float32), 29)
roundedCurve.addNode(np.array([  10,  -20,  0.5], dtype = np.float32), 30)
roundedCurve.addNode(np.array([  0,  -2,    0.5], dtype = np.float32), 31)

# Calculamos la velocidad de nuestros nodos
roundedCurve.updateVelocitiesAndDistances()

# Actualiza la GPUShape de la curva:
roundedCurve.updateShape([1, 1, 1])

# Actualiza la triangleMesh con las vecindades de cada triangulo
roundedCurve.updateTriangleMeshVecinity()

# Buscamos el triangulo en el que esta el auto en un principio
roundedCurve.firstCarSearch()

"""
________________________________________________________________________________
VIEW: Dibujo y pick-up de keyboard events
________________________________________________________________________________
"""

if __name__ == "__main__":

    # Initialize glfw
    if not glfw.init():
        sys.exit()

    width = 2000
    height = 980

    window = glfw.create_window(width, height, "Crazy Racer", None, None)

    if not window:
        glfw.terminate()
        sys.exit()

    glfw.make_context_current(window)

    # Connecting the callback function 'on_key' to handle keyboard events
    glfw.set_key_callback(window, on_key)

    # Assembling the shader program
    pipeline = ls.SimplePhongShaderProgram()
    glUseProgram(pipeline.shaderProgram)

    # Setting up the clear screen color
    glClearColor(0.15, 0.15, 0.15, 1.0)

    # As we work in 3D, we need to check which part is in front,
    # and which one is at the back
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_BLEND)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

    # Creating shapes on GPU memory
    # gpuSurface = toGPUShape(cpuSurface, GL_REPEAT, GL_NEAREST)
    carNode = createCar(0, 0.32, 0.12)
    gpuMesh = es.toGPUShape(roundedCurve.Shape)

    t0 = glfw.get_time()

    while not glfw.window_should_close(window):
        # Using GLFW to check for input events
        glfw.poll_events()

        # Getting the time difference from the previous iteration
        t1 = glfw.get_time()
        controller.dt = t1 - t0
        t0 = t1

        if (glfw.get_key(window, glfw.KEY_LEFT) == glfw.PRESS):
            controller.autoPhi += 3 * controller.dt

        elif (glfw.get_key(window, glfw.KEY_RIGHT) == glfw.PRESS):
            controller.autoPhi -= 3 * controller.dt

        if (glfw.get_key(window, glfw.KEY_UP) == glfw.PRESS):
            controller.autoPos[0] += 27 * atAuto[0] * controller.dt
            controller.autoPos[1] += 27 * atAuto[1] * controller.dt

        elif (glfw.get_key(window, glfw.KEY_DOWN) == glfw.PRESS):
            controller.autoPos[0] -= 27 * atAuto[0] * controller.dt
            controller.autoPos[1] -= 27 * atAuto[1] * controller.dt

        # Filling or not the shapes depending on the controller state
        if (controller.fillPolygon):
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)
        else:
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)

        # Setting up the view transform
        atAuto = [np.cos(controller.autoPhi), np.sin(controller.autoPhi), controller.autoPos[2]]
        eyeCamera = np.array([controller.autoPos[0] - (atAuto[0] * 15), controller.autoPos[1] - (atAuto[1] * 15), controller.autoPos[2] + 6])
        # Camara estilo GTA
        # eyeCamera = np.array([controller.autoPos[0] - 10, controller.autoPos[1] - 10, controller.autoPos[2] * 4])
        atCamera = np.array([controller.autoPos[0] + atAuto[0], controller.autoPos[1] + atAuto[1], controller.autoPos[2]])

        view = tr.lookAt(
            eyeCamera,
            atCamera,
            np.array([0, 0, 1])
        )

        # Buscamos el auto en los triangulos vecinos
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[1])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[2])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[1].nearTriangles[1])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[2].nearTriangles[1])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[1].nearTriangles[2])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[2].nearTriangles[2])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[1].nearTriangles[1].nearTriangles[1])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[1].nearTriangles[1].nearTriangles[2])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[2].nearTriangles[1].nearTriangles[1])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[2].nearTriangles[1].nearTriangles[2])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[1].nearTriangles[2].nearTriangles[1])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[1].nearTriangles[2].nearTriangles[2])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[2].nearTriangles[2].nearTriangles[1])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[2].nearTriangles[2].nearTriangles[2])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[1].nearTriangles[1].nearTriangles[1].nearTriangles[1])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[1].nearTriangles[1].nearTriangles[1].nearTriangles[2])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[1].nearTriangles[1].nearTriangles[2].nearTriangles[1])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[1].nearTriangles[1].nearTriangles[2].nearTriangles[2])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[2].nearTriangles[1].nearTriangles[1].nearTriangles[1])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[2].nearTriangles[1].nearTriangles[1].nearTriangles[2])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[2].nearTriangles[1].nearTriangles[2].nearTriangles[1])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[2].nearTriangles[1].nearTriangles[2].nearTriangles[2])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[1].nearTriangles[2].nearTriangles[1].nearTriangles[1])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[1].nearTriangles[2].nearTriangles[1].nearTriangles[2])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[1].nearTriangles[2].nearTriangles[2].nearTriangles[1])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[1].nearTriangles[2].nearTriangles[2].nearTriangles[2])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[2].nearTriangles[2].nearTriangles[1].nearTriangles[1])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[2].nearTriangles[2].nearTriangles[1].nearTriangles[2])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[2].nearTriangles[2].nearTriangles[2].nearTriangles[1])
        roundedCurve.searchForCar(controller.carTriangle.nearTriangles[2].nearTriangles[2].nearTriangles[2].nearTriangles[2])

        # Clearing the screen in both, color and depth
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

        # Dibujamos la pista curva
        projection = tr.perspective(60, float(width)/float(height), 0.1, 400)
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "view"), 1, GL_TRUE, view)
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "projection"), 1, GL_TRUE, projection)
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "model"), 1, GL_TRUE, tr.identity())

        glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "La"), 1.0, 1.0, 1.0)
        glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Ld"), 1.0, 1.0, 1.0)
        glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Ls"), 1.0, 1.0, 1.0)

        glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Ka"), 0.6, 0.6, 0.6)
        glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Kd"), 0.9, 0.9, 0.9)
        glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Ks"), 1.0, 1.0, 1.0)

        glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "lightPosition"), 8, 8, 8)
        glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "viewPosition"), eyeCamera[0], eyeCamera[1], eyeCamera[2])
        glUniform1ui(glGetUniformLocation(pipeline.shaderProgram, "shininess"), 100)
        glUniform1f(glGetUniformLocation(pipeline.shaderProgram, "constantAttenuation"), 0.001)
        glUniform1f(glGetUniformLocation(pipeline.shaderProgram, "linearAttenuation"), 0.1)
        glUniform1f(glGetUniformLocation(pipeline.shaderProgram, "quadraticAttenuation"), 0.01)
        pipeline.drawShape(gpuMesh)

        # Ligting stuff
        glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "La"), 1.0, 1.0, 1.0)
        glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Ld"), 1.0, 1.0, 1.0)
        glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Ls"), 1.0, 1.0, 1.0)

        glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Ka"), 0.6, 0.6, 0.6)
        glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Kd"), 0.9, 0.9, 0.9)
        glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "Ks"), 1.0, 1.0, 1.0)

        glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "lightPosition"), 8, 8, 8)
        glUniform3f(glGetUniformLocation(pipeline.shaderProgram, "viewPosition"), eyeCamera[0] , eyeCamera[1], eyeCamera[2])
        glUniform1ui(glGetUniformLocation(pipeline.shaderProgram, "shininess"), 100)
        glUniform1f(glGetUniformLocation(pipeline.shaderProgram, "constantAttenuation"), 0.001)
        glUniform1f(glGetUniformLocation(pipeline.shaderProgram, "linearAttenuation"), 0.1)
        glUniform1f(glGetUniformLocation(pipeline.shaderProgram, "quadraticAttenuation"), 0.01)

        # car transformations
        carNode.transform = tr.matmul([tr.translate(controller.autoPos[0], controller.autoPos[1], controller.autoPos[2]), tr.uniformScale(1.7), tr.rotationZ(controller.autoPhi)])
        redWheelRotationNode = sg.findNode(carNode, "wheelRotation")
        redWheelRotationNode.transform = tr.rotationY(-10 * glfw.get_time())

        # Drawing car
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "projection"), 1, GL_TRUE, projection)
        glUniformMatrix4fv(glGetUniformLocation(pipeline.shaderProgram, "view"), 1, GL_TRUE, view)
        sg.drawSceneGraphNode(carNode, pipeline, "model")

        # Once the drawing is rendered, buffers are swap so an uncomplete drawing is never seen.
        glfw.swap_buffers(window)

    glfw.terminate()
